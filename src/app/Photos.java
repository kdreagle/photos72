package app;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;

import javafx.application.Application;
import javafx.stage.Stage;
import object.*;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * The Photos class is the main class that runs the 
 */
public class Photos extends Application implements Serializable {
	
	/**
	 * Main stage used by all controllers
	 */
	public static Stage mainStage;
	
	/**
	 * The directory where all application data is stored
	 */
	public static final String storeDir = "data";
	
	/**
	 * The file where all user data is stored
	 */
	public static final String storeFile = "users2.dat";
	
	/**
	 * The current user using the application
	 */
	public static User currentUser;
	
	/**
	 * The current album the user is looking at
	 */
	public static Album currentAlbum;
	
	/**
	 * The current index of the current album
	 */
	public static int currentAlbumIndex;
	
	/**
	 * The current photo the user is looking at
	 */
	public static Photo currentPhoto;
	
	/**
	 * The index of the current photo
	 */
	public static int currentPhotoIndex;
	
	/**
	 * The string that the user entered to search for a photo
	 */
	public static String searchQuery;
	
	/**
	 * The starting date of the date range of photos the user is looking for
	 */
	public static Date startQuery;
	
	/**
	 * The ending date of the date range of photos the user is looking for
	 */
	public static Date endQuery;
	
	/**
	 * The list of photos that matched the search query
	 */
	public static ArrayList<Photo> lastSearchedResults;
	
	/**
	 * Specifies if the new album is one made of search results
	 */
	public static boolean searchedAlb; 
	
	/**
	 * ArrayList that holds all user data
	 */
	public static ArrayList<User> users;
	
	/**
	 * This method executes when the application starts
	 * @param primaryStage
	 * @throws Exception
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {	
		
		readUsers();
		
		mainStage = primaryStage;
		
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
		
		mainStage.setScene(new Scene(pane,900,600));
		mainStage.show();
		
	}
	
	/**
	 * This method executes when the application is closed
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@Override
	public void stop() throws FileNotFoundException, IOException{
	    writeUsers();
	}
	
	/**
	 * Reads the user data from file
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static void readUsers() throws IOException, ClassNotFoundException {
		try {
			ObjectInputStream ois;
			ois = new ObjectInputStream(
					new FileInputStream(storeDir + File.separator + storeFile));
			users = (ArrayList<User>)ois.readObject();
			
			if (users == null)
				users = new ArrayList<User>();
			
			ois.close();
		} catch (FileNotFoundException e) {
			users = new ArrayList<User>();
		}
		
	}
	
	/**
	 * Writes the user data to file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void writeUsers() throws FileNotFoundException, IOException {
		ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(storeDir + File.separator + storeFile));
		oos.writeObject(users); 
		oos.close();
		
	}
	
	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
	/**
	 * Creates a warning pop up
	 * @param contentText Details of the warning
	 */
	public static void warning(String contentText) {
		Alert warning = new Alert(AlertType.WARNING);
		warning.initOwner(mainStage);
		warning.setTitle("Oops!");
		warning.setHeaderText(contentText);
		warning.showAndWait();
	}
	
}
