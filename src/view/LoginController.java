package view;

import java.io.IOException;
import app.Photos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import object.*;


/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * This class handles all login logic
 */
public class LoginController {
	
	/**
	 * Text box for entering username
	 */
	@FXML
	TextField username;
	
	/**
	 * Initializes the login view by setting the title of the window and sets the 
	 * prompt text for the username text box
	 */
	@FXML
	public void initialize() {
		Photos.mainStage.setTitle("Login");
		
		username.setPromptText("Enter username here");
	}
	
	/**
	 * Sends user to the page with all their albums
	 * @param event Event when user clicks login button
	 */
	@FXML
	private void login(ActionEvent event) throws IOException {
		
		String user = username.getText();
		
		boolean userExists = false;
		
		for (User tempUser : Photos.users) {
			if (tempUser.username.equals(user)) {
				Photos.currentUser = tempUser;
				userExists = true;
			}
		}
		
		if (!userExists && !user.toLowerCase().equals("admin")) {
			Photos.currentUser = new User(user);
			Photos.users.add(Photos.currentUser);
		}
		
		AnchorPane pane;
		
		if (user.toLowerCase().equals("admin")) 
			pane = FXMLLoader.load(getClass().getResource("/view/admin.fxml"));
		else
			pane = FXMLLoader.load(getClass().getResource("/view/albums.fxml"));
		
		
		Photos.mainStage.setScene(new Scene(pane,900,600));
		
	}

}
