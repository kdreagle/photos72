package view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.TimeUnit;

import app.Photos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import object.*;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * Controller for adding a new photo
 */
public class AddPhotoController {
	
	/**
	 * Text box for entering the caption for the photo
	 */
	@FXML
	private TextField caption; 
	
	/**
	 * The file of the photo to add
	 */
	private File selectedPhoto;
	
	/**
	 * Initializes the title of the window and the text field prompt
	 */
	@FXML
	public void initialize() {
		Photos.mainStage.setTitle("Add photo");
		
		caption.setPromptText("Enter caption here");
	}
	
	/**
	 * Opens the FileChooser so the user can select a photo from their file system
	 * @param event Event when user clicks the choose photo button
	 */
	@FXML
	private void choosePhoto(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Photo");
		fileChooser.setSelectedExtensionFilter(new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
		selectedPhoto = fileChooser.showOpenDialog(Photos.mainStage);
		caption.setText(selectedPhoto.getName());
		
	}

	/**
	 * Sends user back to the photo list view with the new photo
	 * @param event Event when user clicks confirm button
	 */
	@FXML
	private void confirm(ActionEvent event) throws IOException {
		if(caption.getText().indexOf("STOCKPHOTO") != -1) {
			Photos.currentAlbum.photos.add(new Photo(caption.getText().substring(caption.getText().indexOf("STOCKPHOTO") + 10),selectedPhoto.getName()));
		} else {
			Photos.currentAlbum.photos.add(new Photo(caption.getText(),"file:" + selectedPhoto.toPath().toString()));
		}
		System.out.println(selectedPhoto.getName());
		
		
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/photos.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
		Photos.mainStage.centerOnScreen();
	}
	
	/**
	 * Sends user back to the photo list view without the new photo
	 * @param event Event when user clicks cancel button
	 */
	@FXML
	private void cancel(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/photos.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
		Photos.mainStage.centerOnScreen();
	}
}
