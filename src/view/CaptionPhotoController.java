package view;

import java.io.IOException;

import app.Photos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import object.*;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * Controller for captioning or recaptioning a photo
 */
public class CaptionPhotoController {
	
	/**
	 * Text box for entering the new caption of a photo
	 */
	@FXML
	private TextField caption;
	
	/**
	 * Initializes the text box with the previous photo caption
	 */
	@FXML
	public void initialize() {
		caption.setText(Photos.currentPhoto.caption);
	}

	/**
	 * Sends user back to the photo list view with the new caption
	 * @param event Event when user clicks confirm button
	 */
	@FXML
	private void confirm(ActionEvent event) throws IOException {
		
		Photos.currentPhoto.caption = caption.getText();
		
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/photos.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
		Photos.mainStage.centerOnScreen();
	}
	
	/**
	 * Sends user back to the photo list view without a new caption
	 * @param event Event when user clicks canel button
	 */
	@FXML
	private void cancel(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/photos.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
		Photos.mainStage.centerOnScreen();
	}
}
