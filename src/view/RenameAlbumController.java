package view;

import java.io.IOException;

import app.Photos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import object.*;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * Controller for adding a new album
 */
public class RenameAlbumController {
	
	/**
	 * Text box for entering the new name of the album
	 */
	@FXML
	private TextField albumName;
	
	/**
	 * Initializes the title of the window and the text field prompt
	 */
	@FXML
	public void initialize() {
		Photos.mainStage.setTitle("Add tag");
		
		albumName.setText(Photos.currentAlbum.name);
	}
	
	/**
	 * Sends user back to the album list view with the new album name
	 * @param event Event when user clicks confirm button
	 */
	@FXML
	private void confirm(ActionEvent event) throws IOException {
		
		Photos.currentAlbum.name = albumName.getText();
		boolean repeated = false;
		for(Album album : Photos.currentUser.albums) {
			if(album.name.equals(albumName.getText())) {
				repeated = true;
				break;
			}
		} 
		if (!repeated)Photos.currentUser.albums.add(new Album(albumName.getText()));
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/albums.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
		Photos.mainStage.centerOnScreen();
	}
	
	/**
	 * Sends user back to the album list view without the new album
	 * @param event Event when user clicks cancel button
	 */
	@FXML
	private void cancel(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/albums.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
		Photos.mainStage.centerOnScreen();
	}
}
