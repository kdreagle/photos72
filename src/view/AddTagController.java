package view;

import java.io.IOException;

import app.Photos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import object.*;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * Controller for adding a new tag
 */
public class AddTagController {
	
	/**
	 * Text box for entering a new tag name
	 */
	@FXML
	private TextField tagName;
	
	@FXML
	private TextField tagValue;
	
	
	/**
	 * Initializes the title of the window and the text field prompt
	 */
	@FXML
	public void initialize() {
		Photos.mainStage.setTitle("Add tag");
		
		tagName.setPromptText("Enter tag name here");
		tagValue.setPromptText("Enter tag value here");
	}

	
	/**
	 * Sends user back to the slideshow with the new tag
	 * @param event Event when user clicks confirm button
	 */
	@FXML
	private void confirm(ActionEvent event) throws IOException {
		
		Photos.currentPhoto.tags.put(tagName.getText(),tagValue.getText());
		Photos.currentPhoto.madeChange();
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/slideshow.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
		Photos.mainStage.centerOnScreen();
	}
	
	/**
	 * Sends user back to the slideshow without the new tag
	 * @param event Event when user clicks cancel button
	 */
	@FXML
	private void cancel(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/slideshow.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
		Photos.mainStage.centerOnScreen();
	}
}
