package view;

import java.io.IOException;

import app.Photos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * This class handles all slideshow logic
 */
public class SlideshowController {

	/**
	 * Button for going back the the photo list window
	 */
	@FXML
	Button backButton;
	
	/**
	 * Button for going to the previous photo in the album
	 */
	@FXML
	Button previousButton;
	
	/**
	 * Button for going to the next photo in the album
	 */
	@FXML
	Button nextButton;
	
	/**
	 * The photo currently shown
	 */
	@FXML
	ImageView photo;
	
	/**
	 * ButtonBar for holding the photo's tags
	 */
	@FXML
	ButtonBar tags;
	
	/**
	 * The photo's caption
	 */
	@FXML
	Text caption;
	
	/**
	 * The photo's date
	 */
	@FXML
	Text date;
	
	/**
	 * Initializes the slideshow with the tags of the photo and their respective actions
	 */
	@FXML
	public void initialize() {
		Photos.mainStage.setTitle(Photos.currentPhoto.caption);
		
		caption.setText(Photos.currentPhoto.caption);
		date.setText(Photos.currentPhoto.date.getTime().toString());
		
		backButton.setGraphic(new ImageView(new Image("left_32.png")));
		previousButton.setGraphic(new ImageView(new Image("left_32.png")));
		nextButton.setGraphic(new ImageView(new Image("right_32.png")));
		
		Button buttonTag;
		for (String tag : Photos.currentPhoto.tags.keySet()) {
			buttonTag = new Button(tag + " : " +  Photos.currentPhoto.tags.get(tag));
			ContextMenu contextMenu = new ContextMenu();
			MenuItem delete = new MenuItem("Delete");
			delete.setOnAction(deleteEvent -> {
				Photos.currentPhoto.tags.remove(tag);
				AnchorPane pane;
				try {
					pane = FXMLLoader.load(getClass().getResource("/view/slideshow.fxml"));
					Photos.mainStage.setScene(new Scene(pane,900,600));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			contextMenu.getItems().addAll(delete);
			buttonTag.setContextMenu(contextMenu);
			
			buttonTag.setOnAction(event -> {
				Photos.searchQuery = tag;
				AnchorPane pane;
				try {
					pane = FXMLLoader.load(getClass().getResource("/view/searchresults.fxml"));
					Photos.mainStage.setScene(new Scene(pane,900,600));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			tags.getButtons().add(buttonTag);
		}
		
		
		
		photo.setImage(new Image(Photos.currentPhoto.path));
	}
	
	/**
	 * Go back to the previous page
	 * @param event Event when user clicks back button
	 */
	@FXML
	private void back(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/photos.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
	}
	
	/**
	 * Go to the next photo in the album
	 * @param event Event when user clicks back button
	 */
	@FXML
	private void nextPhoto(ActionEvent event) throws IOException {
		if (Photos.currentPhotoIndex + 1 < Photos.currentAlbum.photos.size()) {
			Photos.currentPhotoIndex++;
			Photos.currentPhoto = Photos.currentAlbum.photos.get(Photos.currentPhotoIndex);
			AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/slideshow.fxml"));
			Photos.mainStage.setScene(new Scene(pane,900,600));
		}
	}
	
	/**
	 * Go back to the previous page
	 * @param event Event when user clicks back button
	 */
	@FXML
	private void previousPhoto(ActionEvent event) throws IOException {
		if (Photos.currentPhotoIndex - 1 >= 0) {
			Photos.currentPhotoIndex--;
			Photos.currentPhoto = Photos.currentAlbum.photos.get(Photos.currentPhotoIndex);
			AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/slideshow.fxml"));
			Photos.mainStage.setScene(new Scene(pane,900,600));
		}

	}
	
	/**
	 * Switches scene to add tag view
	 * @param event Event when user clicks Add Tag button
	 */
	@FXML
	private void addTag(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/addtag.fxml"));
		Photos.mainStage.setScene(new Scene(pane,400,200));
		Photos.mainStage.centerOnScreen();
	}
	
	/**
	 * Switches scene to move photo view
	 * @param event Event when user clicks move to button
	 */
	@FXML
	private void moveTo(ActionEvent event) throws IOException {
		CopyPhotoController.move = true;
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/copyphoto.fxml"));
		Photos.mainStage.setScene(new Scene(pane,400,600));
		Photos.mainStage.centerOnScreen();
	}
	
	/**
	 * Switches scene to copy photo view
	 * @param event Event when user clicks copy to button
	 */
	@FXML
	private void copyTo(ActionEvent event) throws IOException {
		CopyPhotoController.move = false;
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/copyphoto.fxml"));
		Photos.mainStage.setScene(new Scene(pane,400,600));
		Photos.mainStage.centerOnScreen();
	}
	
	/**
	 * Brings user back to login screen
	 * @param event Event when user clicks logout button
	 */
	@FXML
	private void logout(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
	}
}
