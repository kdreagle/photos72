package view;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import app.Photos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import object.*;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * This class handles all internal album logic
 */
public class PhotosController {
	
	/**
	 * Holds all photos with their captions for visibility
	 */
	@FXML
	TilePane photos;
	
	/**
	 * Button for going back the the album list window
	 */
	@FXML
	Button backButton;
	
	/**
	 * ImageView for each photo
	 */
	ImageView FXPhoto;
	
	/**
	 * Initializes the photos window with all the photos and their respective context menus
	 */
	@FXML
	public void initialize() {
		Photos.mainStage.setTitle(Photos.currentAlbum.name);
		
		backButton.setGraphic(new ImageView(new Image("left_32.png")));
		
	    
		photos.setHgap(8);
		photos.setPrefColumns(4);
		
		//photos.setAlignment(Pos.CENTER);
		//photos.setTileAlignment(Pos.CENTER);
		
		GridPane photoPane;
		
		
		Text caption;
		Text photoDate;
		
		Image image;
		
		if (Photos.currentAlbum.photos.isEmpty() == false) {
			for (Photo photo: Photos.currentAlbum.photos) {
				photoPane = new GridPane();
	
				image = new Image(photo.path);
				FXPhoto = new ImageView(image);
	
				FXPhoto.setFitHeight(150);
				FXPhoto.setPreserveRatio(true);
				
				FXPhoto.setOnContextMenuRequested(event -> {
					ContextMenu contextMenu = new ContextMenu();
					MenuItem delete = new MenuItem("Delete");
					MenuItem recaption;
					if (photo.caption.equals("")) 
						recaption = new MenuItem("Caption");
					else
						recaption = new MenuItem("Recaption");
					MenuItem copy = new MenuItem("Copy to...");
					MenuItem move = new MenuItem("Move to...");
					copy.setOnAction(moveEvent -> {
						Photos.currentPhoto = photo;
						CopyPhotoController.move = false;
						AnchorPane pane;
						try {
							pane = FXMLLoader.load(getClass().getResource("/view/copyphoto.fxml"));
							Photos.mainStage.setScene(new Scene(pane,400,600));
							Photos.mainStage.centerOnScreen();
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
					move.setOnAction(moveEvent -> {
						Photos.currentPhoto = photo;
						CopyPhotoController.move = true;
						AnchorPane pane;
						try {
							pane = FXMLLoader.load(getClass().getResource("/view/copyphoto.fxml"));
							Photos.mainStage.setScene(new Scene(pane,400,600));
							Photos.mainStage.centerOnScreen();
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
					recaption.setOnAction(captionEvent -> {
						AnchorPane pane;
						Photos.currentPhoto = photo;
						try {
							pane = FXMLLoader.load(getClass().getResource("/view/captionphoto.fxml"));
							Photos.mainStage.setScene(new Scene(pane,400,200));
							Photos.mainStage.centerOnScreen();
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
					delete.setOnAction(deleteEvent -> {
						Photos.currentAlbum.photos.remove(photo);
						AnchorPane pane;
						try {
							pane = FXMLLoader.load(getClass().getResource("/view/photos.fxml"));
							Photos.mainStage.setScene(new Scene(pane,900,600));
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
					contextMenu.getItems().addAll(copy,move,recaption,delete);
					contextMenu.show(FXPhoto, event.getScreenX(), event.getScreenY());
				});
				
				FXPhoto.setOnMouseClicked(event -> {
					if (event.getButton() == MouseButton.PRIMARY) {
						AnchorPane pane;
						Photos.currentPhoto = photo;
						Photos.currentPhotoIndex = Photos.currentAlbum.photos.indexOf(photo);
						try {
							pane = FXMLLoader.load(getClass().getResource("/view/slideshow.fxml"));
							Photos.mainStage.setScene(new Scene(pane,900,600));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
				
				caption = new Text(photo.caption);
				caption.setTextAlignment(TextAlignment.CENTER);
				photoDate = new Text(photo.date.getTime().toString());
				photoDate.setTextAlignment(TextAlignment.CENTER);
				photoPane.add(FXPhoto, 0, 0);
				photoPane.add(caption, 0, 1);
				photoPane.setAlignment(Pos.CENTER);
				photos.getChildren().add(photoPane);
			}
		}
	}
	
	/**
	 * Go back to the previous page
	 * @param event Event when user clicks back button
	 */
	@FXML
	private void back(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/albums.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
	}
	
	/**
	 * Scene switches to the add photo view
	 * @param event Event when user clicks Add Photo button
	 */
	@FXML
	private void addPhoto(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/addphoto.fxml"));
		Photos.mainStage.setScene(new Scene(pane,400,200));
		Photos.mainStage.centerOnScreen();
	}
	
	/**
	 * The current album is deleted and the scene is switched back to the album list view
	 * @param event Event when user clicks Delete Album button
	 */
	@FXML
	private void deleteAlbum(ActionEvent event) throws IOException {
		Photos.currentUser.albums.remove(Photos.currentAlbum);
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/albums.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
	}
	
	/**
	 * Brings user back to login screen
	 * @param event Event when user clicks logout button
	 */
	@FXML
	private void logout(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
	}

}
