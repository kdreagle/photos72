package view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import app.Photos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Text;
import object.*;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * The controller for showing search results
 */
public class SearchResultsController {
	
	/**
	 * Holds all photos with their captions for visibility
	 */
	@FXML
	TilePane photos;
	
	/**
	 * Button for going back the the album list window
	 */
	@FXML
	Button backButton;
	
	/**
	 * ImageView for each photo
	 */
	ImageView FXPhoto;
	
	/**
	 * Initializes the window with all photos relating to the search query
	 */
	@FXML
	public void initialize() {
		Photos.mainStage.setTitle("Search results");
		
		backButton.setGraphic(new ImageView(new Image("left_32.png")));
		
	    
		photos.setHgap(8);
		photos.setPrefColumns(4);
		
		if(Photos.lastSearchedResults  == null) Photos.lastSearchedResults = new ArrayList<>();
		
		GridPane photoPane;
		
		Text caption;
		
		Image image;
		
		
		if(Photos.searchQuery.equals("")){
		for (Album album: Photos.currentUser.albums) {
			for(Photo photo: album.photos) {
				Photos.lastSearchedResults.add(photo);
			}
			}
		} else {
			getTaggedImages();
		}
		
		checkDates();
		
				for (Photo photo: Photos.lastSearchedResults) {
						photoPane = new GridPane();
						
						image = new Image(photo.path);
						FXPhoto = new ImageView(image);
			
						FXPhoto.setFitHeight(150);
						FXPhoto.setPreserveRatio(true);
						
						caption = new Text(photo.caption);
						photoPane.add(FXPhoto, 0, 0);
						photoPane.add(caption, 0, 1);
						photos.getChildren().add(photoPane);
					
				}
	}
	
	/**
	 * Creates a new album that contains the search results
	 * @param event Event when user clicks back button
	 */
	@FXML
	private void createAlbum(ActionEvent event) throws IOException {
		
		Photos.searchedAlb = true;
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/addalbum.fxml"));
		Photos.mainStage.setScene(new Scene(pane,400,200));
		Photos.mainStage.centerOnScreen();
		
	}
	
	/**
	 * Go back to the previous page
	 * @param event Event when user clicks back button
	 */
	@FXML
	private void back(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/albums.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
	}
	
	
	/**
	 * Brings user back to login screen
	 * @param event Event when user clicks logout button
	 */
	@FXML
	private void logout(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
	}
	
	/**
	 * Gathers images that are tagged with the requested tag(s)
	 */
	public void getTaggedImages() {
		HashMap<String,String> tagsToSearch = new HashMap<>();
		System.out.println( Photos.searchQuery);
		String[] splitTags = Photos.searchQuery.split("\\s+");
		for(int i =0; i < splitTags.length ; i++) {
			String cur = splitTags[i];
			System.out.println(cur);
			if(cur.indexOf("=")!= -1) {
				//System.out.println(cur.substring(0,cur.indexOf("=")) + " " +  cur.substring(cur.indexOf("=")+1));
				tagsToSearch.put(cur.substring(0,cur.indexOf("=")), cur.substring(cur.indexOf("=")+1));
			}
		}
		
		ArrayList<Photo> photosMatch = new ArrayList<>();
		for (Album album: Photos.currentUser.albums) {
			for(Photo photo: album.photos) {
				for(String tag : photo.tags.keySet()) {
					if(tagsToSearch.containsKey(tag)) {
						if(tagsToSearch.get(tag).equals(photo.tags.get(tag))) {
							photosMatch.add(photo);
						}
					}
				}
			}
		}
		Photos.lastSearchedResults = photosMatch;
	}
	
	/**
	 * Finds photos that match the given date range
	 */
	public void checkDates() {
		ArrayList<Photo> photosMatch = new ArrayList<>();
		if(Photos.startQuery != null && Photos.endQuery != null) {
			for (Photo photo: Photos.lastSearchedResults) {
				if(photo.date.getTime().compareTo(Photos.startQuery) >= 0 && photo.date.getTime().compareTo(Photos.endQuery) <= 0) {
					photosMatch.add(photo);
				}
			}
			Photos.lastSearchedResults = photosMatch;
		}
		else if(Photos.endQuery != null) {
			for(int i =0; i < Photos.lastSearchedResults.size();i++) {
				Photo photo = Photos.lastSearchedResults.get(i);
				if(photo.date.getTime().compareTo(Photos.endQuery) <= 0) {
					photosMatch.add(photo);
				}
				System.out.println("d");
			}
			Photos.lastSearchedResults = photosMatch;
		}
		else if(Photos.startQuery != null){
			for (Photo photo: Photos.lastSearchedResults) {
				if(photo.date.getTime().compareTo(Photos.startQuery) >= 0) {
					photosMatch.add(photo);
				}
			}
			Photos.lastSearchedResults = photosMatch;
		}
	}

}
