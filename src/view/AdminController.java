package view;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import app.Photos;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import object.User;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * This class handles all administrator logic
 */
public class AdminController {
	
	/**
	 * The title of the current window
	 */
	@FXML
	Text title;
	
	/**
	 * The name of the user to enter
	 */
	@FXML
	TextField username;
	
	/**
	 * The list of users
	 */
	@FXML
	ListView<User> listView;
	
	/**
	 * The list of how many albums each user has
	 */
	@FXML
	ListView<Integer> albumListView;
	
	@FXML
	public void initialize() throws ClassNotFoundException, IOException {
		refresh();
	}
	ObservableList<User> obUsers = FXCollections.observableArrayList(Photos.users);
	ObservableList<Integer> obAlbumNum;
	
	/**
	 * Logic used for logging out
	 * @param event Event when user clicks logout button
	 */
	@FXML
	private void logout(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
	}
	
	/**
	 * Adds a user to the list of users
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void addUser(ActionEvent event) throws IOException {

		String user = username.getText();
		if (user.toLowerCase().equals("admin")) {
			Photos.warning("cannot add admin as a user");
			return;
		}
		for (User tempUser : Photos.users) {
			if (tempUser.username.toLowerCase().equals(user.toLowerCase())) {
				Photos.warning("cannot add duplicate user");
				return;
			}
		}
		Photos.users.add((new User(user)));
		refresh();
	}
	
	/**
	 * Deletes the selected user
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void deleteUser(ActionEvent event) throws IOException {
		int index = listView.getSelectionModel().getSelectedIndex();
		if (!Photos.users.isEmpty() && index >= 0) {
			int selectIndex = listView.getSelectionModel().getSelectedIndex();
			Photos.users.remove(index);
			refresh();
			if(Photos.users.size() <= selectIndex)listView.getSelectionModel().select(selectIndex-1);
			else listView.getSelectionModel().select(selectIndex);
		}
	}
	
	/**
	 * Updates the ListView with all users
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void refresh() throws FileNotFoundException, IOException {
		Photos.writeUsers();
		obUsers = FXCollections.observableArrayList(Photos.users);
		ArrayList<Integer> albumNum = new ArrayList<>();
		for(User user : Photos.users) {
			albumNum.add(user.albums.size());
		}
		obAlbumNum = FXCollections.observableArrayList(albumNum);
		listView.setItems(obUsers);
		
		albumListView.setItems(obAlbumNum);
		
	}
}
