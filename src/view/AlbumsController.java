package view;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import app.Photos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.DialogPane;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import object.*;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * This class handles all album logic
 */
public class AlbumsController {
	
	/**
	 * The pane that holds all the album covers and album names
	 */
	@FXML
	TilePane albums;
	
	/**
	 * The search box for searching for photos by tag
	 */
	@FXML
	TextField searchTag;
	
	/**
	 * DatePicker for the earliest date of a range of photos
	 */
	@FXML
	DatePicker beginDate;
	
	/**
	 * DatePicker for the latest date of a range of photos
	 */
	@FXML
	DatePicker endDate;
	
	/**
	 * ImageView for the album cover
	 */
	ImageView albumCover;
	
	/**
	 * Initializes the album list view by filling the tile pane with album covers and their names.
	 * Context menus are created for each album as well
	 */
	@FXML
	public void initialize() {
		Photos.mainStage.setTitle("Good evening, " + Photos.currentUser.username);
		searchTag.setPromptText("Search by tags (e.g. location=paris)");
		beginDate.setPromptText("Start date (MM/DD/YYYY)");
		endDate.setPromptText("End date (MM/DD/YYYY)");
	    
		albums.setHgap(8);
		albums.setPrefColumns(4);
		
		
		
		GridPane albumPane;
		
		Text albumName;
		
		if (Photos.currentUser.albums.size() != 0) {
			for (Album album: Photos.currentUser.albums) {
				albumPane = new GridPane();
	
				albumCover = new ImageView(new Image("folder_64.png"));
				
				albumCover.setOnContextMenuRequested(event -> {
					ContextMenu contextMenu = new ContextMenu();
					MenuItem rename = new MenuItem("Rename");
					MenuItem delete = new MenuItem("Delete");
					rename.setOnAction(renameEvent -> {
						Photos.currentAlbum = album;
						AnchorPane pane;
						try {
							pane = FXMLLoader.load(getClass().getResource("/view/renamealbum.fxml"));
							Photos.mainStage.setScene(new Scene(pane,400,200));
							Photos.mainStage.centerOnScreen();
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
					delete.setOnAction(deleteEvent -> {
						Photos.currentUser.albums.remove(album);
						AnchorPane pane;
						try {
							pane = FXMLLoader.load(getClass().getResource("/view/albums.fxml"));
							Photos.mainStage.setScene(new Scene(pane,900,600));
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
					contextMenu.getItems().addAll(rename,delete);
					contextMenu.show(albumCover, event.getScreenX(), event.getScreenY());
				});
				albumCover.setOnMouseClicked(event -> {
					if (event.getButton() == MouseButton.PRIMARY) {
						AnchorPane pane;
						Photos.currentAlbum = album;
						try {
							pane = FXMLLoader.load(getClass().getResource("/view/photos.fxml"));
							Photos.mainStage.setScene(new Scene(pane,900,600));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
				albumName = new Text(album.name);
				albumPane.add(albumCover, 0, 0);
				albumPane.add(albumName, 0, 1);
				albums.getChildren().add(albumPane);
			}
		}
		
	}
	
	/**
	 * Brings user back to login screen
	 * @param event Event when user clicks logout button
	 */
	@FXML
	private void logout(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
	}
	
	
	/**
	 * Scene switches to add album view
	 * @param event Event when user clicks Add Album button
	 */
	@FXML
	private void addAlbum(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/addalbum.fxml"));
		Photos.mainStage.setScene(new Scene(pane,400,200));
		Photos.mainStage.centerOnScreen();
	}
	
	/**
	 * Scene switches to add album view
	 * @param event Event when user clicks Add Album button
	 */
	@FXML
	private void search(ActionEvent event) throws IOException {
		
		LocalDate localDate = beginDate.getValue();
		Instant instant;
		if(localDate != null) {
			instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			Photos.startQuery = Date.from(instant);
		} else {
			Photos.startQuery = null;
		}
		localDate = endDate.getValue();
		if(localDate != null) {
			instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			Photos.endQuery = Date.from(instant);
		} else {
			Photos.endQuery = null;
		}
		
		
		Photos.searchQuery = searchTag.getText();
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/searchresults.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
	}
	

	
	
}
