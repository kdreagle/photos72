package view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import app.Photos;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import object.*;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * Controller for copying a photo to another album
 */
public class CopyPhotoController {
	
	/**
	 * ListView that shows the albums that the user can move the photo to
	 */
	@FXML
	ListView albums;
	
	/**
	 * Boolean to for other controllers to tell this controller whether 
	 * the photo is going to be copied or moved.
	 */
	static boolean move;

	/**
	 * Initializes the ListView with the albums to 
	 */
	@FXML
	public void initialize() {
		ObservableList<String> albumList = FXCollections.observableArrayList();
		
		for (Album album: Photos.currentUser.albums) {
			if (!album.name.equals(Photos.currentAlbum.name))
				albumList.add(album.name);
		}
		
		albums.setItems(albumList);
	}
	
	/**
	 * Sends user back to the photo list view with the new photo
	 * @param event Event when user clicks confirm button
	 */
	@FXML
	private void confirm(ActionEvent event) throws IOException {
		
		String copyToAlbum = (String) albums.getSelectionModel().getSelectedItem();
		
		for (Album album: Photos.currentUser.albums) {
			if (album.name.equals(copyToAlbum))  {
				album.photos.add(Photos.currentPhoto);
				if (move) Photos.currentAlbum.photos.remove(Photos.currentPhoto);
				Photos.currentAlbum = album;
				break;
			}
		}
		
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/photos.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
		Photos.mainStage.centerOnScreen();
	}
	
	/**
	 * Sends user back to the photo list view without the new photo
	 * @param event Event when user clicks cancel button
	 */
	@FXML
	private void cancel(ActionEvent event) throws IOException {
		AnchorPane pane = FXMLLoader.load(getClass().getResource("/view/photos.fxml"));
		Photos.mainStage.setScene(new Scene(pane,900,600));
		Photos.mainStage.centerOnScreen();
	}
}
