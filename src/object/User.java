package object;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * Holds user data
 */
public class User implements Serializable{
	
	/**
	 * The user's username
	 */
	public String username;
	
	/**
	 * All of the user's albums
	 */
	public ArrayList<Album> albums;
	
	public User(String username) {
		this.username = username;
		this.albums = new ArrayList<Album>();
	}
	
	/**
	 * Returns the username of the user
	 */
	public String toString() {
		return username;
	}
}
