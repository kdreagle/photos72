package object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * Holds user photo data
 */
public class Photo implements Serializable {

	/**
	 * The caption of the photo
	 */
	public String caption;
	
	/**
	 * The path of the photo on the user's PC
	 */
	public String path;
	
	/**
	 * All the tags of the photo
	 */
	public Map<String, String> tags;
	
	/**
	 * The date the photo was uploaded
	 */
	public Calendar date;
	
	public Photo(String caption, String path) {
		this.caption = caption;
		this.path = path;
		this.tags = new HashMap<String, String>();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		this.date = cal;
		
		
	}
	
	/**
	 * Changes the date of the photo
	 */
	public void madeChange() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND, 0);
		this.date = cal;
	}
}
