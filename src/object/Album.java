package object;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Matthew Skrobola , Kyle Reagle
 * <p>
 * Holds user album data
 */
public class Album implements Serializable {
	
	/**
	 * The name of the album
	 */
	public String name;
	
	/**
	 * All photos in the album
	 */
	public ArrayList<Photo> photos;
	
	public Album(String name) {
		this.name = name;
		this.photos = new ArrayList<Photo>();
	}
}
