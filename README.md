# Important notes for grader #

- Some features may only be accessible through right clicking, such as deleting a tag or recaptioning a photo or renaming an album, however this feature should be apparent from the storyboard
- We don't know what happened to the initial commit of the storyboard and worksplit documents, but you should have gotten an email from bitbucket before October 29th when we both committed them.